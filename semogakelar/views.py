from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection
import psycopg2
from django.views.decorators.csrf import csrf_exempt

#----------Fitur13_CUDPengiriman----------#
@csrf_exempt
def insert_new_pengiriman(request):
    if request.method == 'POST':
        record_to_insert_barang = (
            generate_no_resi(),
            get_id_pemesanan(request.POST['barang_pesanan']),
            request.POST['metode'],
            generate_ongkos(),
            request.POST['tanggal_pengiriman'],
            get_no_ktp_anggota(),
            get_nama_alamat_anggota()
        )

        pg_insert_query_barang = """INSERT INTO TOY_RENT.PENGIRIMAN(no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) VALUES (%s,%s,%s,%s,%s,%s,%s) """

    with connection.cursor() as cursor:
        cursor.execute(pg_insert_query_barang, record_to_insert_barang)

    print(request.POST)

    context = {
        'operation': 'CREATE : id_barang ' + request.POST['id_barang']
    }
    return render(request, 'cud_response.html', context)

def get_nama_alamat_anggota():
    return null

def get_no_ktp_anggota():
    return null

def generate_ongkos():
    return 10000 #promo flat ongkir

def get_id_pemesanan(barang_pesanan):
    return null

def generate_no_resi():
    no_resi = 0
    with connection.cursor() as cursor:
        cursor.execute("SELECT no_resi from TOY_RENT.PENGIRIMAN")
        data = cursor.fetchall()
        if len(data) != 0:
            no_resi = len(data)
        cursor.close()

    return str(no_resi)
