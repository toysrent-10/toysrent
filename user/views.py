from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection
from .Forms import ItemForm, Login, SignUp, ItemFormUpdate
import psycopg2
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect

response = {}

def profile(request, ktp):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.pengguna where No_ktp = '" + ktp + "'")
        data = cursor.fetchall()
        cursor.close()
    response['user'] = data
    return render(request, 'profile.html', response)

def login(request):
    response['fill_form'] = Login
    return render(request, 'login.html', response)

def create(request):
    response['fill_form'] = ItemForm
    return render(request, 'create.html', response)

def daftar(request):
    response['barang'] = get_all_barang()
    return render(request, 'daftar_item.html', response)

def index(request):
    return render(request, 'index.html', response)

def signup(request):
    response['fill_form'] = SignUp
    return render(request, 'sign up.html', response)

def update(request, namaBarang):
    response['fill_form'] = ItemFormUpdate
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.item")
        data = cursor.fetchall()
        cursor.close()
    response['up'] = data
    return render(request, 'update.html', response)

def get_all_barang():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.kategori_item")
        data = cursor.fetchall()
        cursor.close()
    print(data)
    return data

@csrf_exempt
def post_item_admin(request):
    if request.method == 'POST':
        print(request.POST)
        saveThisItem = (request.POST['nama'], request.POST['deskripsi'],
                    request.POST['usiaMin'], request.POST['usiaMax'],
                    request.POST['bahan'])
        saveThisKategori = (saveThisItem[0], request.POST['kategori'])
        pg_insert_query_item = """INSERT INTO toy_rent.item(Nama, Deskripsi, Usia_dari, Usia_Sampai, Bahan) VALUES (%s,%s,%s,%s,%s)"""
        pg_insert_query_kategori_item = """INSERT INTO toy_rent.kategori_item(Nama_item, Nama_kategori) VALUES (%s,%s)"""
        with connection.cursor() as cursor:
            cursor.execute(pg_insert_query_item, saveThisItem)
            cursor.execute(pg_insert_query_kategori_item, saveThisKategori)
        return HttpResponseRedirect('/user/daftar')

@csrf_exempt
def loginCheck(request):
    if request.method == 'POST':
        checkThis = request.POST['ktp']
        with connection.cursor() as cursor:
            pg_insert_query_user = "SELECT * FROM toy_rent.pengguna where No_ktp = '" + checkThis + "'"
            cursor.execute(pg_insert_query_user)
            data = cursor.fetchall()
            cursor.close()
        print(data)
        if (len(data) > 0):
            return HttpResponseRedirect('/user/daftar')
        else:
            return HttpResponseRedirect('/user/login')

def checkSignUp(request):
    ktp = request.POST['ktp']
    signThis = (request.POST['ktp'], request.POST['nama'], request.POST['email'], request.POST['tanggalLahir'], request.POST['telepon'])
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.pengguna where No_ktp = '" + ktp + "'")
        data = cursor.fetchall()
        cursor.close()
    if (len(data) == 0):
        pg_insert_query_pengguna = """INSERT INTO toy_rent.pengguna(No_ktp, Nama_lengkap, Email, Tanggal_lahir, No_telp) VALUES (%s,%s,%s,%s,%s)"""
        with connection.cursor() as cursor:
            cursor.execute(pg_insert_query_pengguna, signThis)
        return HttpResponseRedirect('/user/daftar')
    else:
        return HttpResponseRedirect('/user/login')

def deleteItem(request, namaBarang):
    with connection.cursor() as cursor:
        cursor.execute("DELETE from toy_rent.kategori_item where nama_item= '" + namaBarang + "'")
        cursor.execute("DELETE from toy_rent.item where nama= '" + namaBarang + "'")
        cursor.close()
    return HttpResponseRedirect("/user/daftar")

@csrf_exempt
def updateItem(request):
    updateThisItem = (request.POST['deskripsi'],
                    request.POST['usiaMin'], request.POST['usiaMax'],
                    request.POST['bahan'])
    pg_update_query_item = """UPDATE toy_rent.item set deskripsi = %s , usia_dari = %s , usia_sampai = %s , bahan = %s"""
    with connection.cursor() as cursor:
        cursor.execute(pg_update_query_item, updateThisItem)
    return HttpResponseRedirect("/user/daftar/")