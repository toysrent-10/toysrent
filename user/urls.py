from django.urls import path, re_path
from . import views

urlpatterns = [
    path('<str:ktp>', views.profile, name='profile'),
    path('login/', views.login, name='login'),
    path('create/', views.create, name='create'),
    path('daftar/', views.daftar, name='daftar'),
    path('index/', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('update/<str:namaBarang>/', views.update, name='update'),
    path('create_item/', views.post_item_admin, name='createItem'),
    path('update_item/', views.updateItem, name='updateItem'),
    path('check_login/', views.loginCheck, name='checkLogin'),
    path('check_signup/', views.checkSignUp, name='checkSignUp'),
    path('delete/<str:namaBarang>/', views.deleteItem, name='delete')
]
