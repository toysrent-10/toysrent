from django import forms

class ItemForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    nama = forms.CharField(label = "Nama", required = True, widget = forms.TextInput(attrs = attrs))
    deskripsi = forms.CharField(label = "Deskripsi Singkat", required = True, widget = forms.TextInput(attrs = attrs))
    usiaMin = forms.IntegerField(label = "Usia Minimal", required = True, widget = forms.TextInput(attrs = attrs))
    usiaMax = forms.IntegerField(label="Usia Maksimal", required=True, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label="Kategori", required=True, widget=forms.TextInput(attrs=attrs))
    bahan = forms.CharField(label="Bahan", required=True, widget=forms.TextInput(attrs=attrs))

class ItemFormUpdate(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    nama = forms.CharField(label = "Nama", required = True, widget = forms.TextInput(attrs = attrs))
    deskripsi = forms.CharField(label = "Deskripsi Singkat", required = True, widget = forms.TextInput(attrs = attrs))
    usiaMin = forms.IntegerField(label = "Usia Minimal", required = True, widget = forms.TextInput(attrs = attrs))
    usiaMax = forms.IntegerField(label="Usia Maksimal", required=True, widget=forms.TextInput(attrs=attrs))
    bahan = forms.CharField(label="Bahan", required=True, widget=forms.TextInput(attrs=attrs))

class Login(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    ktp = forms.CharField(label="No. KTP", required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.CharField(label="Email", required=True, widget=forms.TextInput(attrs=attrs))

class SignUp(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    ktp = forms.CharField(label = "No. KTP", required = True, widget = forms.TextInput(attrs = attrs))
    nama = forms.CharField(label = "Nama Lengkap", required = True, widget = forms.TextInput(attrs = attrs))
    email = forms.CharField(label = "Email", required = True, widget = forms.TextInput(attrs = attrs))
    tanggalLahir = forms.CharField(label="Tanggal Lahir (Gunakkan format yyyy-mm-dd)", required=True, widget = forms.TextInput(attrs = attrs))
    telepon = forms.CharField(label="No. Telepon", required=True, widget=forms.TextInput(attrs=attrs))
    alamat = forms.CharField(label="Alamat", required=True, widget=forms.TextInput(attrs=attrs))
