from django.urls import path

from . import views

urlpatterns = [
    path('<str:no_ktp>/', views.show_chat)
]