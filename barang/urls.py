from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('post/new_barang', views.post_barang_admin),
    path('form/new_barang', views.add_new_barang_form),
    path('detail/<str:id_barang>', views.show_detail_barang),
    path('delete/<str:id_barang>', views.delete_barang),
    path('form/update/<str:id_barang>', views.update_barang_form),
    path('update/barang', views.update_barang_admin)
]