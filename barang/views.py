from django.shortcuts import render
from django.http import HttpResponse

from django.db import connection
import psycopg2

# Create your views here.
from django.views.decorators.csrf import csrf_exempt


def get_all_barang():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.barang")
        data = cursor.fetchall()
        cursor.close()

    return data


def get_nama_by_ktp(no_ktp):
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_lengkap FROM toy_rent.pengguna WHERE no_ktp = '" + no_ktp + "'")
        data = cursor.fetchone()
        cursor.close()

    return data[0]


def get_barang_by_id(id_bar):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.barang WHERE id_barang = '" + id_bar + "'")
        data = cursor.fetchall()
        list_tup = list(data[0])
        list_tup[6] = get_nama_by_ktp(list_tup[6])

        data[0] = list_tup
        cursor.close()

    return data


def get_info_level_barang_by_id(id_bar):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toy_rent.info_barang_level WHERE id_barang = '" + id_bar + "'")
        data = cursor.fetchall()
        cursor.close()

    return data


def get_review_barang_by_id(id_bar):
    with connection.cursor() as cursor:
        cursor.execute("SELECT review,tanggal_review FROM toy_rent.barang_dikirim WHERE id_barang = '" + id_bar + "'")
        data = cursor.fetchall()
        cursor.close()

    return data


def index(request):
    context = {
        'list_barang': get_all_barang()
    }

    return render(request, 'show_daftar_barang.html', context)


def show_detail_barang(request, id_barang):
    context = {
        'detail_barang': get_barang_by_id(id_barang),
        'list_info_level_barang': get_info_level_barang_by_id(id_barang),
        'list_review_barang': get_review_barang_by_id(id_barang)

    }

    return render(request, 'detail_barang.html', context)


def add_new_barang_form(request):
    context = {
        'list_item': get_name_all_item(),
        'list_pengguna': get_name_all_pengguna()
    }

    return render(request, 'form_penambahan_barang.html', context)


def update_barang_form(request, id_barang):
    context = {
        'list_item': get_name_all_item(),
        'list_pengguna': get_name_all_pengguna(),
        'detail_data': get_barang_by_id(id_barang),
        'list_info_barang': get_info_level_barang_by_id(id_barang)
    }

    return render(request, 'form_update_barang.html', context)


def get_name_all_item():
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama FROM toy_rent.item")
        data = cursor.fetchall()
        cursor.close()

    return data


def get_name_all_pengguna():
    with connection.cursor() as cursor:
        cursor.execute("SELECT DISTINCT nama_lengkap FROM toy_rent.pengguna NATURAL JOIN toy_rent.anggota")
        data = cursor.fetchall()
        cursor.close()

    return data


def get_certain_anggota(nama):
    with connection.cursor() as cursor:
        cursor.execute("SELECT no_ktp FROM toy_rent.pengguna WHERE nama_lengkap = '" + nama + "'")
        data = cursor.fetchone()
        cursor.close()

    return data


def delete_barang(request, id_barang):
    with connection.cursor() as cursor:
        cursor.execute("DELETE from toy_rent.barang where id_barang= '" + id_barang + "'")
        cursor.close()

    context = {
        'operation': 'DELETE : id_barang ' + id_barang
    }
    return render(request, 'info_crud.html', context)


@csrf_exempt
def post_barang_admin(request):
    if request.method == 'POST':
        record_to_insert_barang = (
            request.POST['id_barang'],
            request.POST['nama_item'],
            request.POST['warna'],
            request.POST['url_foto'],
            request.POST['kondisi'],
            request.POST['lama_penggunaan'],
            get_certain_anggota(request.POST['nama_penyewa'])
        )
        record_to_insert_info_barang_level_bronze = (
            request.POST['id_barang'],
            ' BRONZE',
            request.POST['harga_bronze'],
            request.POST['persen_bronze']
        )
        record_to_insert_info_barang_level_silver = (
            request.POST['id_barang'],
            ' SILVER',
            request.POST['harga_silver'],
            request.POST['persen_silver']
        )
        record_to_insert_info_barang_level_gold = (
            request.POST['id_barang'],
            ' GOLD',
            request.POST['harga_gold'],
            request.POST['persen_gold']
        )

        pg_insert_query_barang = """INSERT INTO toy_rent.barang(id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) VALUES (%s,%s,%s,%s,%s,%s,%s) """
        pg_insert_query_info_barang_level = """INSERT INTO toy_rent.info_barang_level(id_barang, nama_level, harga_sewa, porsi_royalti) VALUES (%s,%s,%s,%s)"""
    with connection.cursor() as cursor:
        cursor.execute(pg_insert_query_barang, record_to_insert_barang)
        cursor.execute(pg_insert_query_info_barang_level, record_to_insert_info_barang_level_bronze)
        cursor.execute(pg_insert_query_info_barang_level, record_to_insert_info_barang_level_silver)
        cursor.execute(pg_insert_query_info_barang_level, record_to_insert_info_barang_level_gold)

    print(request.POST)

    context = {
        'operation': 'CREATE : id_barang ' + request.POST['id_barang']
    }
    return render(request, 'info_crud.html', context)


@csrf_exempt
def update_barang_admin(request):

    record_to_update_barang = (
        request.POST['nama_item'],
        request.POST['warna'],
        request.POST['url_foto'],
        request.POST['kondisi'],
        request.POST['lama_penggunaan'],
        get_certain_anggota(request.POST['nama_penyewa']),
        request.POST['id_barang']
    )
    record_to_update_info_barang_level_bronze = (
        ' BRONZE',
        request.POST['harga_bronze'],
        request.POST['persen_bronze'],
        request.POST['id_barang']
    )

    record_to_update_info_barang_level_gold = (
        ' GOLD',
        request.POST['harga_gold'],
        request.POST['persen_gold'],
        request.POST['id_barang']
    )

    pg_update_query_barang = """UPDATE toy_rent.barang set nama_item = %s , warna = %s , url_foto = %s , kondisi = %s , lama_penggunaan = %s , no_ktp_penyewa = %s WHERE id_barang = %s """
    pg_update_query_info_barang_level = """UPDATE toy_rent.info_barang_level set harga_sewa = % , porsi_royalti = %s WHERE id_barang = %s """

    with connection.cursor() as cursor:
        cursor.execute(pg_update_query_barang, record_to_update_barang)
        cursor.execute("UPDATE toy_rent.info_barang_level set harga_sewa =" + request.POST['harga_bronze'] + " , porsi_royalti = " + request.POST['persen_bronze']
                       + " WHERE id_barang = '" + request.POST['id_barang'] + "' AND nama_level = ' BRONZE'")
        cursor.execute("UPDATE toy_rent.info_barang_level set harga_sewa =" + request.POST['harga_silver'] +" , porsi_royalti = "+ request.POST['persen_silver']
                       +" WHERE id_barang = '" + request.POST['id_barang']+"' AND nama_level = ' SILVER'")
        cursor.execute("UPDATE toy_rent.info_barang_level set harga_sewa =" + request.POST['harga_gold'] + " , porsi_royalti = " + request.POST['persen_gold']
                       + " WHERE id_barang = '" + request.POST['id_barang'] + "' AND nama_level = ' GOLD'")

    print(request.POST)

    context = {
        'operation': 'UPDATE : id_barang ' + request.POST['id_barang']
    }
    return render(request, 'info_crud.html', context)
